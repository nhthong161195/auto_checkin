const rp = require('request-promise');
const data = require('./data.json')

const getAuthToken = async (username, password) => {
    let jsonRequest = {action: "get_oauth_token", grantType: "password", grantData: [username, password]}
    let uri = 'identity uri'
    let options = {
        method: 'POST',
        uri: uri,
        body: {
            ...jsonRequest
        },
        json: true
    };
    let result = await rp(options);
    return result
}

const getLoginToken = async (loginToken) => {
    let jsonRequest = {
        action_name: "get_access_token",
        loginToken: loginToken
    }
    let uri = 'checkin uri'
    let options = {
        method: 'POST',
        uri: uri,
        body: {
            ...jsonRequest
        },
        json: true
    };
    let result = await rp(options)
    return result
}

const checkIn = async (checkinToken) => {
    let jsonRequest = {action_name: "claim_for_presence"}
    let uri = 'checkin uri'
    let options = {
        method: 'POST',
        uri: uri,
        headers: {
            Authorization: checkinToken
        },
        body: {
            ...jsonRequest
        },
        json: true // Automatically stringifies the body to JSON
    };
    let result = await rp(options)
    return result
}

const main = async (username, password) => {

    let accessToken = await getAuthToken(username, password);
    let loginToken = accessToken["result"]["token"]
    let checkinToken = await getLoginToken(loginToken)
    let authToken = checkinToken["result"]["token"]
    let result = await checkIn(authToken);
    console.log('result checkin', result)
}

(
  async () => {
      for (let i = 0; i < data.length; i ++) {
          let user = data[i];
          await main(user.username, user.password)
      }
  }
)().catch(err => console.log("error", err))
