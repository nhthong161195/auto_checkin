
1. Install nodejs : https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-18-04

2. Install pm2 : https://pm2.keymetrics.io/docs/usage/quick-start/

3. git clone project and run commands:
    - npm install
    
    - pm2 start index.js --cron "0 8 * * 1-5"